FROM httpd:2.4

LABEL maintainer="Dhruvi Patel"
LABEL email="dhruvipatel2004@gmail.com"
LABEL version="0.1"

RUN apt-get -y update
RUN apt -y install mysql-server
COPY index.html /usr/local/apache2/htdocs
COPY startup.sh /startup.sh
RUN chmod +x /startup.sh

ENTRYPOINT ["/startup.sh"]
